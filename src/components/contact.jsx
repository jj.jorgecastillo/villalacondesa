import { useEffect } from "react";
import { Form, Input, Button, notification, Row, Col, Spin } from "antd";

import { FrownTwoTone, SendOutlined, SmileOutlined } from "@ant-design/icons";

import mailgo from "mailgo";

const validateMessages = {
  required: "${label} Es requerido!", // eslint-disable-line
  types: {
    email: "${label} No es un correo válido!", // eslint-disable-line
    number: "${label} Solo se permiten números!", // eslint-disable-line
  },
};

const { TextArea } = Input;
const { Item } = Form;

export const Contact = (props) => {
  useEffect(() => {
    mailgo(
      (mailgo.MailgoConfig = {
        dark: true,
        lang: "es",
        es: {
          open_in_: "abrir con ",
          bcc_: "cco ",
          subject_: "asunto ",
          body_: "cuerpo ",
          call: "llamar",
          open: "abrir",
          _default: " predefinido",
          _as_default: " por defecto",
          copy: "copiar",
          copied: "copiado",
        },
      })
    );
  }, []);
  const [form] = Form.useForm();

  const onFinish = ({ nombre, email, telefono, mensaje }) => {
    const formData = new FormData();

    formData.append("nombre", nombre);
    formData.append("email", email);
    formData.append("telefono", telefono);
    formData.append("servicio", mensaje);
    formData.append("_template", "table");
    formData.append("_captcha", false);
    formData.append("_cc", "jj.jorgecastillo@gmail.com");

    fetch("https://formsubmit.co/lacondesachinacota@gmail.com", {
      method: "POST",
      mode: "no-cors",
      headers: {
        ContentType: "application/multipart/form-data",
      },
      body: formData,
    })
      .then(() => {
        notification.open({
          type: "success",
          icon: (
            <SmileOutlined
              style={{
                color: "#108ee9",
              }}
            />
          ),
          message: "Mensaje enviado con éxito",
          duration: 5,
          description: `${nombre} Pronto nos comunicaremos contigo.`,
          placement: "bottomLeft",
        });
        form.resetFields();
      })
      .catch(() => {
        notification.open({
          type: "error",
          icon: <FrownTwoTone twoToneColor="#eb2f96" />,
          message: "Mensaje No enviado ",
          duration: 5,
          description: `${nombre} Ocurrió un error, intenta nuevamente.`,
          placement: "bottomLeft",
        });
      });
  };

  return (
    <div>
      <div id="contact">
        <div className="container">
          <div className="col-md-8">
            <div className="row">
              <div className="section-title">
                <h2>Póngase en contacto con nosotros.</h2>
                <p>
                  Por favor, rellene el siguiente formulario para enviarnos un
                  correo electrónico y le daremos de vuelta a usted tan pronto
                  como sea posible.
                </p>
              </div>

              <Form
                id="VillaLaCondesa"
                size="large"
                form={form}
                onFinish={onFinish}
                validateMessages={validateMessages}
                layout="vertical"
              >
                <Row gutter={[8, 16]}>
                  <Col xs={24} md={8}>
                    <Item name="nombre" rules={[{ required: true }]}>
                      <Input placeholder="NOMBRE" />
                    </Item>
                  </Col>
                  <Col xs={24} md={8}>
                    <Item
                      name="email"
                      rules={[{ required: true, type: "email" }]}
                    >
                      <Input placeholder="CORREO ELECTRÓNICO" />
                    </Item>
                  </Col>
                  <Col xs={24} md={8}>
                    <Item name="telefono" rules={[{ required: true }]}>
                      <Input placeholder="TELÉFONO" />
                    </Item>
                  </Col>
                  <Col xs={24}>
                    <Item name="mensaje" rules={[{ required: true }]}>
                      <TextArea
                        autoSize={{ minRows: 2, maxRows: 3 }}
                        placeholder="MENSAJE"
                        showCount
                        maxLength={250}
                      />
                    </Item>
                  </Col>
                </Row>
                <div className="text-center">
                  <Button
                    className="btn btn-custom btn-lg"
                    icon={<SendOutlined />}
                    type="primary"
                    shape="round"
                    htmlType="submit"
                  >
                    Enviar mensaje
                  </Button>
                </div>
              </Form>
            </div>
          </div>

          <div className="col-md-3 col-md-offset-1 contact-info">
            <div className="contact-item">
              <h3>Información de contacto</h3>
              <p>
                <span>
                  <i className="fa fa-map-marker"></i> Dirección
                </span>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://www.google.com/maps/dir//Villa+La+Condesa+Chin%C3%A1cota,+Norte+de+Santander/@7.5718736,-72.5862559,16z/data=!4m8!4m7!1m0!1m5!1m1!1s0x8e662fae42eb4241:0xbc7cc4b0d3a755ac!2m2!1d-72.5862559!2d7.5718736"
                  style={{ color: "#FAFA80", fontWeight: 800 }}
                >
                  {props.data ? props.data.address : <Spin tip="Cargando..." />}
                </a>
              </p>
            </div>
            <div className="contact-item">
              <p>
                <span>
                  <i className="fa fa-phone"></i> Teléfono
                </span>{" "}
                <a
                  data-telegram="telegram"
                  href={`tel:${props?.data?.phone}`}
                  style={{ color: "#FAFA80", fontWeight: 800 }}
                >
                  {props.data ? props.data.phone : <Spin tip="Cargando..." />}
                </a>
              </p>
            </div>
            <div className="contact-item">
              <p>
                <span>
                  <i className="fa fa-envelope-o"></i> Correo electrónico
                </span>{" "}
                <a
                  href={`mailto:${props?.data?.email}`}
                  style={{ color: "#FAFA80", fontWeight: 800 }}
                >
                  {props.data ? props.data.email : <Spin tip="Cargando..." />}
                </a>
              </p>
            </div>
          </div>
          <div className="col-md-12">
            <div className="row">
              <div className="social">
                <ul>
                  <li>
                    <a
                      href={props.data ? props.data.facebook : "/"}
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i
                        style={{ color: "#fafafa", background: "#3C5A99" }}
                        className="fa fa-facebook"
                      />
                    </a>
                  </li>
                  <li>
                    <a
                      href={props.data ? props.data.instagram : "/"}
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i
                        style={{
                          color: "#fff",
                          background:
                            "radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%,#d6249f 60%,#285AEB 90%)	",
                        }}
                        className="fa fa-instagram"
                      />
                    </a>
                  </li>
                  <li>
                    <a
                      href={props.data ? props.data.youtube : "/"}
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i
                        style={{ color: "#fafafa", background: "#CE1312" }}
                        className="fa fa-youtube"
                      />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="text-center">
          <a href="https://codecastle.cc/" target="_blank" rel="noreferrer">
            <span style={{ fontSize: 18, color: "#FAFAFA" }}>
              Made with ❤️ by
              <span style={{ fontWeight: 800 }}> {"<"}</span>
              <span style={{ color: "yellow", fontWeight: 800 }}>C</span>
              <span style={{ color: "blue", fontWeight: 800 }}>ode</span>
              <span style={{ color: "yellow", fontWeight: 800 }}>C</span>
              <span style={{ color: "blue", fontWeight: 800 }}>astle </span>
              <span style={{ fontWeight: 800 }}> {"/>"}</span>
            </span>
          </a>
          <br />
          <br />
          <iframe
            title="Chinácota, Villa La Condesa"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4703.349052739279!2d-72.58916979472455!3d7.571778125736685!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbc7cc4b0d3a755ac!2sVilla%20La%20Condesa!5e0!3m2!1ses-419!2sco!4v1643752204730!5m2!1ses-419!2sco&z=11"
            width="100%"
            height="60%"
            frameBorder="0"
            allowFullScreen
            aria-hidden="false"
            loading="lazy"
          />
        </div>
      </div>
    </div>
  );
};
