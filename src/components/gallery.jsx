import { Image as IMG, Col, Row, Typography, Spin } from "antd";
import { Image } from "./image";

export const Gallery = (props) => {
  return (
    <div id="portfolio" className="text-center">
      <div className="container">
        <div className="section-title">
          <h2>Galeria</h2>
          <p>
            Mira las fotos de nuestra galeria, si quieres disfrutar de nuestros
            establecimientos visitanos.
          </p>
        </div>
        <Row gutter={[8, 16]} className="container">
          <IMG.PreviewGroup>
            {props.data ? (
              props.data.map((d, i) => (
                <Col key={`${d.title}-${i}`} xs={24} md={8}>
                  <IMG
                    alt={`${d.title} Villa La Condesa`}
                    width={400}
                    src={d.largeImage}
                    placeholder={
                      <Image preview={false} src={d.smallImage} width={400} />
                    }
                  />
                  <Col xs={24}>
                    <Typography.Text type="secondary" strong>
                      {d.title}
                    </Typography.Text>
                  </Col>
                </Col>
              ))
            ) : (
              <Spin tip="Cargando..." />
            )}
          </IMG.PreviewGroup>
        </Row>
      </div>
    </div>
  );
};
