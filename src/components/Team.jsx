import { Spin } from "antd";

export const Team = (props) => {
  return (
    <div id="team" className="text-center">
      <div className="container">
        <div className="col-md-8 col-md-offset-2 section-title">
          <h2>EQUIPO</h2>
          <p>
            Somos un equipo de profesionales que se esfuerzan por ofrecer un
            servicio de calidad a nuestros clientes, nuestra meta es logra que
            tengas un hermoso recuerdo en tu visita a Chinácota.
          </p>
        </div>
        <div id="row">
          {props.data ? (
            props.data.map((d, i) => (
              <div key={`${d.name}-${i}`} className="col-md-3 col-sm-6 team">
                <div className="thumbnail">
                  {" "}
                  <img
                    src={d.img}
                    alt="Equipo de Villa la condesa Chinácota"
                    className="team-img"
                  />
                  <div className="caption">
                    <h4>{d.name}</h4>
                    <p>{d.job}</p>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <Spin tip="Cargando..." />
          )}
        </div>
      </div>
    </div>
  );
};
