import { Spin } from "antd";

export const Testimonials = (props) => {
  return (
    <div id="testimonials">
      <div className="container">
        <div className="section-title text-center">
          <h2>Lo que dicen nuestros clientes</h2>
          <h3>Deja tu reseña, es importante para nosotros</h3>
          <a
            href="http://search.google.com/local/writereview?placeid=ChIJQULrQq4vZo4RrFWn07DEfLw"
            target="_blank"
            rel="noreferrer"
          >
            Dejar mi testimonio
          </a>
        </div>
        <div className="row">
          {props.data ? (
            props.data.map((d, i) => (
              <div key={`${d.name}-${i}`} className="col-md-4">
                <div className="testimonial">
                  <div className="testimonial-image">
                    {" "}
                    <img src={d.img} alt="" />{" "}
                  </div>
                  <div className="testimonial-content">
                    <p>"{d.text}"</p>
                    <div className="testimonial-meta"> - {d.name} </div>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <Spin tip="Cargando..." />
          )}
        </div>
      </div>
    </div>
  );
};
